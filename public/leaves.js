let gWindowWidth;
let gWindowHeight;

const controls = {};

function cToHex(r, g, b) {
    return `#${r.toString(16)}${g.toString(16)}${b.toString(16)}`;
}

function hextoC(hex) {
    return color(
        parseInt(hex.slice(1, 3), 16),
        parseInt(hex.slice(3, 5), 16),
        parseInt(hex.slice(5, 7), 16),
    );
}

function displayControls() {
    document.getElementById('controls').style.left = `${windowWidth / 2}px`;
    document.getElementById('controls').style.width = `${windowWidth / 2}px`;
    document.getElementById('controls').style.top = `${windowHeight / 2}px`;
    document.getElementById('controls').style.height = `${windowHeight / 2}px`;
    document.getElementById('controls').innerHTML = Object.values(controls).map((control) => {
        const html = `<div class="controldiv"><input type="${control.type === 'color' ? 'color' : 'range'}" id="${control.key}" name="${control.name}" min="${control.min}" max="${control.max}" value="${control.type === 'color' ? cToHex(control.value.levels[0], control.value.levels[1], control.value.levels[2]) : control.value}" />
        <label for="${control.name}">${control.name}</label></div>`;
        return html;
    }).join('<br>');
    Object.values(controls).forEach((control) => {
        document.querySelector('#' + control.key).addEventListener('input', (event) => {
            const value = event.target.value;
            console.log(value);
            if (control.type === 'color') {
                control.value = hextoC(value);
            } else {
                control.value = parseInt(value);
            }
            redraw();
        });
    });
}

function setup() {
    // put setup code here
    gWindowWidth = windowWidth / 2;
    gWindowHeight = windowHeight;
    createCanvas(windowWidth - 1, windowHeight - 1);
    loop();
    controls['width'] = {
        name: 'largeur',
        key: 'width',
        min: '60',
        max: '100',
        value: 100,
    };
    controls['height'] = {
        name: 'hauteur',
        key: 'height',
        min: '60',
        max: '100',
        value: 90,
    };
    controls['firstTierFactor'] = {
        name: 'amincissement de la base',
        key: 'firstTierFactor',
        min: '-30',
        max: '15',
        value: 10,
    };
    controls['secondTierHFactor'] = {
        name: 'angle de la pointe',
        key: 'secondTierHFactor',
        min: '45',
        max: '80',
        value: 50,
    };
    controls['secondTierWFactor'] = {
        name: 'largeur en haut',
        key: 'secondTierWFactor',
        min: '40',
        max: '80',
        value: 50,
    };
    controls['lColor'] = {
        name: 'couleur de feuille',
        key: 'lColor',
        type: 'color',
        value: color(200, 255, 150),
    };
    controls['ltColor'] = {
        name: 'couleur des veines',
        key: 'ltColor',
        type: 'color',
        value: color(255, 200, 200),
    };
    controls['angle'] = {
        name: 'angle',
        key: 'angle',
        min: '175',
        max: '185',
        value: 180,
    };
    displayControls();
}

function c(x, y) {
    return {
        x: x,
        y: y,
    };
}

function ltranslate(coords, x, y) {
    coords.forEach(coord => {
        coord.x = coord.x + x;
        coord.y = coord.y + y;
    });
}

function lrotate(coords, center, angle) {
    coords.forEach(coord => {
        const oldX = coord.x;
        const oldY = coord.y;
        const rad = radians(angle);
        coord.x = cos(rad) * (oldX - center.x) - sin(rad) * (oldY - center.y) + center.x;
        coord.y = sin(rad) * (oldX - center.x) + cos(rad) * (oldY - center.y) + center.y;
    });
}

function lscale(coords, scale) {
    coords.forEach(coord => {
        coord.x = coord.x * scale;
        coord.y = coord.y * scale;
    });
}

function dleaveShape(cv, lColor, ltColor) {
    // leaf blade
    noStroke();
    fill(lColor);
    beginShape();
    vertex(
        cv[0].x, cv[0].y);
    bezierVertex(
        cv[1].x, cv[1].y,
        cv[2].x, cv[2].y,
        cv[3].x, cv[3].y);
    vertex(
        cv[3].x, cv[3].y);
    bezierVertex(
        cv[4].x, cv[4].y,
        cv[5].x, cv[5].y,
        cv[0].x, cv[0].y);
    endShape(CLOSE);
    // midrib
    noFill();
    stroke(ltColor);
    strokeWeight(1);
    curve(
        cv[0].x, cv[0].y,
        cv[0].x, cv[0].y,
        cv[3].x, cv[3].y,
        cv[3].x, cv[3].y);
    // low veins
    strokeWeight(0.8);
    curve(
        cv[0].x, cv[0].y,
        cv[0].x, cv[0].y,
        cv[6].x, cv[6].y,
        cv[6].x, cv[6].y);
    curve(
        cv[0].x, cv[0].y,
        cv[0].x, cv[0].y,
        cv[7].x, cv[7].y,
        cv[7].x, cv[7].y);
    // mid veins
    strokeWeight(0.75);
    curve(
        cv[8].x, cv[8].y,
        cv[8].x, cv[8].y,
        cv[9].x, cv[9].y,
        cv[9].x, cv[9].y);
    curve(
        cv[10].x, cv[10].y,
        cv[10].x, cv[10].y,
        cv[11].x, cv[11].y,
        cv[11].x, cv[11].y);
    // high veins
    strokeWeight(0.5);
    curve(
        cv[12].x, cv[12].y,
        cv[12].x, cv[12].y,
        cv[13].x, cv[13].y,
        cv[13].x, cv[13].y);
    curve(
        cv[14].x, cv[14].y,
        cv[14].x, cv[14].y,
        cv[15].x, cv[15].y,
        cv[15].x, cv[15].y);
}

function dleave(origin, width = 100, height = 90, firstTierFactor = 0.1, secondTierHFactor = 0.5, secondTierWFactor = 0.5, lColor = color(200, 255, 150), ltColor = color(255, 200, 200), angle = 180, scale = 1) {
    const lh = height;
    const alh = lh * firstTierFactor;
    const blh = lh * secondTierHFactor;

    const lw = width;
    const cv = [
        // leaf
        c(0, 0),
        c(-lw / 2, alh),
        c(-lw * secondTierWFactor, blh),
        c(0, lh),
        c(lw * secondTierWFactor, blh),
        c(lw / 2, alh),
        // low veins
        c(-lw / 4, lh * 0.25),
        c(lw / 4, lh * 0.25),
        // mid veins
        c(0, blh / 3),
        c(-lw / 5, lh * 0.5),
        c(0, blh / 3),
        c(lw / 5, lh * 0.5),
        // high veins
        c(0, blh * 0.7),
        c(-lw / 7, lh * 0.7),
        c(0, blh * 0.7),
        c(lw / 7, lh * 0.7),
    ];
    ltranslate(cv, 0, -lh / 2);
    lscale(cv, scale);
    ltranslate(cv, origin.x, origin.y);
    lrotate(cv, origin, angle);
    dleaveShape(cv, lColor, ltColor);
}

function draw() {
    // put drawing code here
    randomSeed(millis() / (1000 * 2))
    background(230, 214, 165);
    maxLeavesW = floor(gWindowWidth / 80);
    maxLeavesH = floor(gWindowHeight / 110);
    for (let cLeavsW = 0; cLeavsW < maxLeavesW; cLeavsW++) {
        for (let cLeavsH = 0; cLeavsH < maxLeavesH; cLeavsH++) {
            const position = c(cLeavsW * 80 + 40, cLeavsH * 110 + 55);
            const height = random(60, 100);
            const width = random(60, 100 * height / 100);
            const firstTierFactor = random(-0.30, 0.15);
            const secondTierHFactor = random(0.45, 0.8);
            const secondTierWFactor = random(0.4, 0.8);
            const lColor = color(240 * random(0.5, 1), 255 * random(0.6, 1), 150 * random(0.5, 1));
            const ltColor = color(150, 100 * random(0.7, 1), 70 * random(0.7, 1));
            const angle = random(175, 185);
            dleave(position, width, height, firstTierFactor, secondTierHFactor, secondTierWFactor, lColor, ltColor, angle);
        }
    }
    stroke(50);
    strokeWeight(2);
    line(windowWidth / 2, 0, windowWidth / 2, windowHeight);
    const position = c(windowWidth * 3 / 4, windowHeight / 4);
    dleave(
        position,
        controls['width'].value,
        controls['height'].value,
        controls['firstTierFactor'].value / 100,
        controls['secondTierHFactor'].value / 100,
        controls['secondTierWFactor'].value / 100,
        controls['lColor'].value,
        controls['ltColor'].value,
        controls['angle'].value,
        (windowHeight / 2 - 5) / 100,
    );
}
