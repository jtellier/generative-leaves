import P5 from "p5";

type Coordinates = {
    active: any;
    x: number,
    y: number,
};

type Item = {
    id: string,
    active: boolean;
    name: string,
    coords: Coordinates[],
    color: string,
    fill: string,
    random: boolean,
}

type State = {
    inventory: Item[],
    grid: boolean,
}

type Store = {
    previousStates: State[],
    futureStates: State[],
    state: State,
}

let changes: boolean = false;
let globalStore: Store = {
    previousStates: [],
    futureStates: [],
    state: {
        inventory: [],
        grid: true,
    }
};
let grid: Coordinates[][] = [];
let gWindowWidth: number;
let gWindowHeight: number;
let refCoordinates: undefined | Coordinates[] = undefined;

function rGS(val: number): number {
    if (globalStore.state.grid) {
        return val - (val % 5);
    } else {
        return val;
    }
}

function saveState() {
    globalStore.previousStates.push(JSON.parse(JSON.stringify(globalStore.state)));
    globalStore.futureStates = [];
    window.localStorage.setItem('store', JSON.stringify(globalStore));
    displayInventory();
}

function restorePreviousState() {
    const previousState = globalStore.previousStates.pop();
    if (previousState) {
        globalStore.futureStates.push(JSON.parse(JSON.stringify(globalStore.state)));
        globalStore.state = previousState;
        window.localStorage.setItem('store', JSON.stringify(globalStore));
        displayInventory();
    }
}

function restoreFutureState() {
    const futureState = globalStore.futureStates.pop();
    if (futureState) {
        globalStore.previousStates.push(JSON.parse(JSON.stringify(globalStore.state)));
        globalStore.state = futureState;
        window.localStorage.setItem('store', JSON.stringify(globalStore));
        displayInventory();
    }
}

function loadInventory() {
    const loadedStringifiedStore: string | null = window.localStorage.getItem('store');
    if (loadedStringifiedStore) {
        try {
            const loadedStore: Store = JSON.parse(loadedStringifiedStore);
            if (loadedStore.hasOwnProperty('previousStates')
                && loadedStore.hasOwnProperty('futureStates')
                && loadedStore.hasOwnProperty('state')
                && loadedStore.state.hasOwnProperty('inventory')) {
                globalStore = loadedStore;
            }
        } catch (e: unknown) {
            console.error('could not load inventory', e);
            console.error(loadedStringifiedStore);
            saveState();
        }
    }
    displayInventory();
}

function drawCoord(coord: Coordinates, p5: P5) {
    p5.fill(255, 255);
    p5.circle(coord.x, coord.y, 7);
}

function cToHex(r: number, g: number, b:number) {
    return `#${r.toString(16)}${g.toString(16)}${b.toString(16)}`;
}

function hextoC(hex: string, p5: P5) {
    return p5.color(
        parseInt(hex.slice(1, 3), 16),
        parseInt(hex.slice(3, 5), 16),
        parseInt(hex.slice(5, 7), 16),
    );
}

function deleteItemFromInventory(item: Item) {
    globalStore.state.inventory = globalStore.state.inventory.filter(currentItem => currentItem.id !== item.id);
    saveState();
}

function selectItemFromInventory(item: Item) {
    if (item.active) {
        globalStore.state.inventory.forEach(currentItem => currentItem.active = false);
    } else {
        globalStore.state.inventory.forEach(currentItem => currentItem.active = false);
        item.active = true;
    }
    saveState();
}

function selectCoord(item: Item, selectedCoord: Coordinates, event: Event) {
    event.stopPropagation();
    if (selectedCoord.active) {
        item.coords.forEach(coord => coord.active = false);
    } else {
        const shapeMoveCheckbox: HTMLInputElement = <HTMLInputElement>document.querySelector('#shape-move');
        const shapeRotateCheckbox: HTMLInputElement = <HTMLInputElement>document.querySelector('#shape-rotate');
        shapeMoveCheckbox.checked = false;
        shapeRotateCheckbox.checked = false;
        item.coords.forEach(coord => coord.active = false);
        selectedCoord.active = true;
    }
    displayInventory();
}

function displayCoord(item: Item, coord: Coordinates, submenu: HTMLElement) {
    const dot = document.createElement('div');
    dot.addEventListener('click', (event) => selectCoord(item, coord, event));
    dot.classList.add('dot');
    if (coord.active) {
        dot.classList.add('active');
    }
    const coordElement = document.createElement('div');
    coordElement.innerHTML = `${coord.x.toFixed(2)} - ${coord.y.toFixed(2)}`;
    dot.appendChild(coordElement);
    submenu.appendChild(dot);
}

function displayInventoryItem(item: Item, inventoryMenu: HTMLElement) {
    const element = document.createElement('div');
    const elementButton = document.createElement('button');
    elementButton.innerHTML = 'X';
    elementButton.addEventListener('click', () => deleteItemFromInventory(item));
    const elementName = document.createElement('span');
    elementName.innerHTML = item.name;
    element.appendChild(elementButton);
    element.appendChild(elementName);
    element.classList.add('item');
    element.addEventListener('click', () => selectItemFromInventory(item));
    if (item.active) {
        element.classList.add('active');
        const submenu = document.createElement('div');
        item.coords.forEach((coord: Coordinates) => displayCoord(item, coord, submenu));
        submenu.classList.add('submenu');
        element.appendChild(submenu);
    }
    inventoryMenu.appendChild(element);
}

function displayInventory() {
    const inventoryMenu: HTMLElement | undefined = <HTMLElement | undefined>document.querySelector('#inventory');
    if (inventoryMenu) {
        inventoryMenu.innerHTML = '';
        globalStore.state.inventory.forEach(item => displayInventoryItem(item, inventoryMenu));
    }
}



function c(x: number, y: number): Coordinates {
    return {
        x: x,
        y: y,
        active: false,
    };
}

function ltranslate(coords: Coordinates[], x: number, y: number): void {
    coords.forEach(coord => {
        coord.x = coord.x + x;
        coord.y = coord.y + y;
    });
}

function coordsRote(coords: Coordinates[], center: Coordinates, angle: number, p5: P5): void {
    coords.forEach((coord: Coordinates) => {
        const oldX = coord.x;
        const oldY = coord.y;
        const rad = p5.radians(angle);
        coord.x = p5.cos(rad) * (oldX - center.x) - p5.sin(rad) * (oldY - center.y) + center.x;
        coord.y = p5.sin(rad) * (oldX - center.x) + p5.cos(rad) * (oldY - center.y) + center.y;
    });
}

function lscale(coords: Coordinates[], scale: number): void {
    coords.forEach((coord: Coordinates) => {
        coord.x = coord.x * scale;
        coord.y = coord.y * scale;
    });
}

const sketch = (p5: P5) => {
    p5.setup = () => {
        // put setup code here
        gWindowWidth = p5.windowWidth;
        gWindowHeight = p5.windowHeight;
        p5.createCanvas(p5.windowWidth - 4, p5.windowHeight - 4);
        p5.loop();
        //displayControls();
        const tools = [
            {
                name: 'bezier',
                coords: [
                    c(gWindowWidth / 2, gWindowHeight / 2 + 0),
                    c(gWindowWidth / 2, gWindowHeight / 2 + 10),
                    c(gWindowWidth / 2, gWindowHeight / 2 + 20),
                    c(gWindowWidth / 2, gWindowHeight / 2 + 30),
                ],
                color: '#000000',
                fill: '#FFFFFF',
                random: false,
            }
        ];
        tools.forEach((tool) => {
            const button = document.createElement('button');
            button.innerHTML = tool.name;
            button.addEventListener('click', () => {
                console.debug('add item: ' + tool.name);
                const item = JSON.parse(JSON.stringify(tool));
                item.active = true;
                item.id = p5.millis() + '|' + p5.random();
                globalStore.state.inventory.forEach(item => item.active = false);
                console.log('item id: ' + item.id);
                globalStore.state.inventory.push(item);
                saveState();
            });
            document.querySelector('#tools')?.appendChild(button);
        });
        loadInventory();
        document.querySelector('#shape-move')?.addEventListener('change', () => {
            const shapeMoveCheckbox: HTMLInputElement | undefined = <HTMLInputElement | undefined>document.querySelector('#shape-move');
            if (shapeMoveCheckbox?.checked) {
                const potentialItem = globalStore.state.inventory
                    .filter(item => item.active)[0];
                if (potentialItem) {
                    potentialItem.coords
                        .forEach(coord => coord.active = false);
                    displayInventory();
                }
            }
        });
        document.querySelector('#shape-rotate')?.addEventListener('change', () => {
            const shapeRotateCheckbox: HTMLInputElement | undefined = <HTMLInputElement | undefined>document.querySelector('#shape-rotate');
            if (shapeRotateCheckbox?.checked) {
                const potentialItem = globalStore.state.inventory
                    .filter(item => item.active)[0];
                if (potentialItem) {
                    potentialItem.coords
                        .forEach(coord => coord.active = false);
                }
            }
        });
        document.querySelector('#shape-copy')?.addEventListener('click', () => {
            const potentialItem: Item | undefined = globalStore.state.inventory
                .filter(item => item.active)[0];
            if (potentialItem) {
                const itemCopy: Item = JSON.parse(JSON.stringify(potentialItem));
                itemCopy.id = p5.millis() + '|' + p5.random();
                ltranslate(itemCopy.coords, 10, 10);
                potentialItem.active = false;
                itemCopy.active = true;
                globalStore.state.inventory.push(itemCopy);
                saveState();
            }
        });
        document.querySelector('#fliph')?.addEventListener('click', () => {
            const potentialItem: Item | undefined = globalStore.state.inventory
                .filter(item => item.active)[0];
            if (potentialItem) {
                const baseX = potentialItem.coords[0].x;
                potentialItem.coords.forEach((coord) => {
                    coord.x -= (coord.x - baseX) * 2;
                });
                saveState();
            }
        });
        document.querySelector('#flipv')?.addEventListener('click', () => {
            const potentialItem: Item | undefined = globalStore.state.inventory
                .filter(item => item.active)[0];
            if (potentialItem) {
                const baseY: number = potentialItem.coords[0].y;
                potentialItem.coords.forEach((coord) => {
                    coord.y -= (coord.y - baseY) * 2;
                });
                saveState();
            }
        });
        document.querySelector('#grid')?.addEventListener('change', () => {
            const shapeRotateCheckbox: HTMLInputElement | undefined = <HTMLInputElement | undefined>document.querySelector('#grid');
            if (shapeRotateCheckbox?.checked) {
                globalStore.state.grid = true;
            } else {
                globalStore.state.grid = false;
            }
            saveState();
        });
        for (let x = 0; x < gWindowWidth; x += 5) {
            grid.push([c(x, 0), c(x, gWindowHeight)]);
        }
        for (let y = 0; y < gWindowWidth; y += 5) {
            grid.push([c(0, y), c(gWindowWidth, y)]);
        }
    }

    p5.draw = () => {
        // put drawing code here
        p5.background(150);
        if (globalStore.state.grid) {
            grid.forEach(lineCoordinates => {
                p5.stroke(200);
                p5.line(lineCoordinates[0].x, lineCoordinates[0].y, lineCoordinates[1].x, lineCoordinates[1].y);
            });
        }
        globalStore.state.inventory.forEach(item => {
            if (item.name === 'bezier') {
                p5.fill(item.fill);
                p5.stroke(item.color);
                p5.beginShape();
                p5.vertex(
                    item.coords[0].x, item.coords[0].y);
                p5.bezierVertex(
                    item.coords[1].x, item.coords[1].y,
                    item.coords[2].x, item.coords[2].y,
                    item.coords[3].x, item.coords[3].y);
                p5.endShape(p5.CLOSE);
            }
        });
        globalStore.state.inventory
            .filter(item => item.active)
            .forEach((item) => {
                item.coords.forEach(coord => {
                    p5.fill(255);
                    if (coord.active) p5.fill(0, 0, 255);
                    p5.circle(coord.x, coord.y, 5);
                });
            });
        if (p5.mouseIsPressed === true) {
            const potentialItem = globalStore.state.inventory
                .filter(item => item.active)[0];
            if (potentialItem) {
                const shapeMoveCheckbox: HTMLInputElement | undefined = <HTMLInputElement | undefined>document.querySelector('#shape-move');
                const shapeRotateCheckbox: HTMLInputElement | undefined = <HTMLInputElement | undefined>document.querySelector('#shape-rotate');
                if (shapeMoveCheckbox?.checked) {
                    if (p5.keyIsDown(p5.SHIFT)) {
                        const baseX = potentialItem.coords[0].x;
                        const baseY = potentialItem.coords[0].y;
                        potentialItem.coords.forEach((coord) => {
                            coord.x -= baseX;
                            coord.y -= baseY;
                        });
                        ltranslate(potentialItem.coords, rGS(p5.mouseX), rGS(p5.mouseY));
                        changes = true;
                    }
                } if (shapeRotateCheckbox?.checked) {
                    if (p5.keyIsDown(p5.SHIFT)) {
                        if (!refCoordinates) {
                            refCoordinates = potentialItem.coords;
                        }
                        const coordsToTransform = JSON.parse(JSON.stringify(refCoordinates));
                        const centerX = potentialItem.coords[0].x;
                        const centerY = potentialItem.coords[0].y;
                        const radians = p5.atan2(p5.mouseX - centerX, p5.mouseY - centerY);
                        const degree = (radians * (180 / Math.PI) * -1) + 90;
                        coordsRote(coordsToTransform, c(centerX, centerY), degree, p5);
                        potentialItem.coords = coordsToTransform;
                        changes = true;
                    }
                } else {
                    const activeDot = potentialItem.coords
                        .filter(coord => coord.active)[0];
                    if (activeDot && p5.keyIsDown(p5.SHIFT)) {
                        activeDot.x = rGS(p5.mouseX);
                        activeDot.y = rGS(p5.mouseY);
                        changes = true;
                    }
                }
            }
        }
    }

    p5.mouseReleased = () => {
        refCoordinates = undefined
    }

    p5.keyPressed = () => {
        if (p5.keyIsDown(p5.CONTROL) && p5.key == 'z') {
            restorePreviousState();
        } else if (p5.keyIsDown(p5.CONTROL) && p5.key == 'y') {
            restoreFutureState();
        }
      
        return false;
    }

    p5.keyReleased = () => {
        if (changes) {
            saveState();
            changes = false;
        }
    }

}

new P5(sketch);
